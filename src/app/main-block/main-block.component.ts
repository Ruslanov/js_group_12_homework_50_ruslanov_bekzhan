import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-main-block',
  templateUrl: './main-block.component.html',
  styleUrls: ['./main-block.component.css']
})
export class MainBlockComponent {
 @Input() title = '';
 @Input() text = '';
}
