import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-move',
  templateUrl: './move.component.html',
  styleUrls: ['./move.component.css']
})
export class MoveComponent  {
  @Input() name = '';
  @Input() info = '';
  @Input() year = '';
  @Input() producer = '';
  @Input() nominations = '';
  @Input() img = '';

}
